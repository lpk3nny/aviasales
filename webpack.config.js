const {resolve} = require('path');
const webpack = require('webpack');
const entry = [];
const plugins = [];
let devtool = 'nosources-source-map';

if (process.env.NODE_ENV == 'development') {
	entry.push(
		'react-hot-loader/patch',
		'webpack-hot-middleware/client'
	);
	plugins.push(
		new webpack.HotModuleReplacementPlugin()
	);
	devtool = 'inline-source-map';
} else {
	plugins.push(
		new webpack.DefinePlugin({
			'process.env': {
				NODE_ENV: JSON.stringify('production')
			}
		}),
		new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /ru/)
	)
}

entry.push('./index.js')
module.exports = {
	context: resolve(__dirname, 'src'),
			 entry,
	devtool: devtool,
	output:  {
		filename:   'bundle.js',
		path:       resolve(__dirname, 'dist'),
		publicPath: '/'
	},
	resolve: {
		modules: [
			resolve(__dirname, "src"),
			"node_modules"
		],
		alias:   {
			'images': resolve(__dirname, "src/images"),
		}
	},
	module:  {
		rules: [
			{
				test:    /\.js$/,
				exclude: /node_modules/,
				use:     [
					'react-hot-loader/webpack',
					'babel-loader',
				]
			},
			{
				test: /\.less$/,
				use:  [
					'style-loader',
					'css-loader',
					'postcss-loader',
					'resolve-url-loader',
					{
						loader:  "less-loader",
						options: {
							sourceMap: true
						}
					}
				],
			},
			{
				test: /\.svg$/i,
				use:  [
					'svg-url-loader',
					'svgo-loader'
				]
			},
		]
	},
			 plugins
};