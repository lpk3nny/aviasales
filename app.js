const express = require('express');
const app = express();

if(process.env.NODE_ENV == 'development') {
    const config = require('./webpack.config.js');
    const compiler = require('webpack')(config);
    app.use(require('webpack-dev-middleware')(compiler, {
        publicPath: config.output.publicPath,
        stats: {
            colors: true
        }
    }));
    app.use(require('webpack-hot-middleware')(compiler));
}

app.use(express.static(__dirname + '/dist'));
app.use(express.static(__dirname + '/static'));

app.get('/', function (req, res) {
    res.sendFile(`${__dirname}/app.html`)
})

app.listen(3000, function () {
    console.log('Example app listening on port 3000!')
})