export function getLabelByStopsCount (count)  {
	switch (parseInt(count)) {
		case 0:
			return 'Без пересадок';
		case 1:
			return '1 пересадка';
		default:
			return `${count} пересадки`;
	}
}