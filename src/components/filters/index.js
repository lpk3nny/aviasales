import React, {Component} from 'react';
import {connect} from 'react-redux';
import Filter from './filter';
import * as actions from 'actions';
import {bindActionCreators} from 'redux';

class Filters extends Component {
	constructor() {
		super();

		this.handleDropAllClick = this.handleDropAllClick.bind(this);
		this.handleHeaderClick = this.handleHeaderClick.bind(this);
	}

	get dropAllClassName() {
		return `filter ${this.props.stopCountStates.filter(filter => filter == false).length ? '' : 'active'}`;
	}

	get className() {
		return `filters ${this.props.ui.expanded ? 'expanded' : ''}`;
	}

	handleDropAllClick(e) {
		e.stopPropagation();

		this.props.toggleAllCountFilters();
	}

	handleHeaderClick() {
		if (window.matchMedia("(max-width: 620px)").matches)
			this.props.UItoggleFilters();
	}

	render() {
		return <aside onClick={this.handleHeaderClick} className={this.className}>
			<h2>Количество пересадок</h2>
			<div className="list">
				<div onClick={this.handleDropAllClick} className={this.dropAllClassName}>Все</div>
				{this.props.stopCountStates.map((state, count) => {
					return <Filter key={count} count={count}/>
				})}
			</div>
		</aside>
	}
}

export default connect(
	(state, ownProps) => state.filters,
	dispatch => {
		return bindActionCreators(Object.assign({}, actions), dispatch)
	}
)(Filters);