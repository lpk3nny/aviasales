import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as actions from 'actions';
import {bindActionCreators} from 'redux';
import {getLabelByStopsCount} from 'utils/string';

class Filter extends Component {
    constructor() {
        super();

        this.handleClick = this.handleClick.bind(this);
        this.handleOnlyClick = this.handleOnlyClick.bind(this);
    }

    get className() {
        return `filter ${this.props.state && 'active'}`;
    }

    get label() {
        return getLabelByStopsCount(this.props.count);
    }

    handleClick(e) {
        e.stopPropagation();

        this.props.toggleStopCountFilter(this.props.count, !this.props.state);
    }

    handleOnlyClick(e) {
        e.stopPropagation();

        this.props.toggleOnlyStopCountFilter(this.props.count, !this.props.state);
    }

    render() {
        return <div onClick={this.handleClick} className={this.className}>
            <span>{this.label}</span>
            <button onClick={this.handleOnlyClick} className="only">только</button>
        </div>
    }
}

export default connect((state, ownProps) => {
    return {
        state: state.filters.stopCountStates[ownProps.count]
    };
}, dispatch => {
    return bindActionCreators(Object.assign({}, actions), dispatch)
})(Filter);