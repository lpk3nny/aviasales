import React, {Component} from 'react';
import Filters from './filters';
import TicketList from './ticketList';

class App extends Component {
    constructor() {
        super();
    }
    render() {
        return <div className="app">
            <div className="logo"></div>
            <div className="content">
                <Filters/>
                <TicketList/>
            </div>
        </div>
    }
}

export default App;