import React, {Component} from 'react';
import {connect} from 'react-redux';
import {map, filter} from 'lodash';
import Ticket from './ticket';
import {fetchTickets} from 'sync/tickets';
import * as actions from 'actions';
import {bindActionCreators} from 'redux';

class TicketList extends Component {
    constructor() {
        super();
    }

    componentDidMount() {
        fetchTickets().then((tickets) => {
            this.props.addTickets(tickets);
        })
    }

    get filteredTickets() {
        let activeCountFilters = [];
        this.props.filters.forEach((state, count) => {
            if (state) activeCountFilters.push(count)
        });

        return filter(this.props.tickets, ticket => activeCountFilters.indexOf(ticket.stops) !== -1);
    }

    render() {
        return <main className="tickets">
            <div className="list">
                {map(this.filteredTickets, ticket => {
                    return <Ticket key={ticket.hash} hash={ticket.hash}/>
                })}
            </div>
        </main>
    }
}

export default connect((state, ownProps) => {
    return {
        filters: state.filters.stopCountStates,
        tickets: state.tickets,
    };
}, dispatch => {
    return bindActionCreators(Object.assign({}, actions), dispatch)
})(TicketList);