import React, {Component} from 'react';
import {connect} from 'react-redux';
import moment from 'moment';
import 'moment/locale/ru';
import {getLabelByStopsCount} from 'utils/string';

class Ticket extends Component {
	constructor() {
		super();
	}

	get formattedDates() {
		const format = (date) => moment(this.props.date).format("D MMM YYYY, dd").replace(/\./g, '');

		return {
			departure: format(this.props.departure_date),
			arrival:   format(this.props.arrival_date),
		}
	}

	get formattedPrice() {
		return this.props.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")
	}

	render() {
		return <section className="ticket">
			<div className="buy-wrapper">
				<div className="carrier-logo">
					<img src={`//pics.avs.io/120/36/${this.props.carrier}.png`}/>
				</div>
				<button className="buy">
					Купить <br/>
					за {this.formattedPrice}
				</button>
			</div>
			<div className="info">
				<div className="departure">
					<div className="time">{this.props.departure_time}</div>
					<div className="location">{this.props.origin}, {this.props.origin_name}</div>
					<div className="date">{this.formattedDates.departure}</div>
				</div>
				<div className="stop-count">
					{getLabelByStopsCount(this.props.stops)}
				</div>
				<div className="arrival">
					<div className="time">{this.props.arrival_time}</div>
					<div className="location">{this.props.destination_name}, {this.props.destination}</div>
					<div className="date">{this.formattedDates.arrival}</div>
				</div>
			</div>
		</section>
	}
}

export default connect((state, ownProps) => {
	return state.tickets[ownProps.hash]
})(Ticket);