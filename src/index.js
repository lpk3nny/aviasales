import './styles/index.less';

import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux'
import {createStore} from 'redux'
import {AppContainer} from 'react-hot-loader';

import App from './components/app';
import reducers from './reducers';
import initialState from './store/initialState';

let store = createStore(
    reducers,
    initialState,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

const render = (Component) => {
    ReactDOM.render(
        <Provider store={store}>
            <AppContainer>
                <Component/>
            </AppContainer>
        </Provider>,
        document.getElementById('root')
    );
};
render(App);

if (module.hot) {
    module.hot.accept('./components/app', () => {
        render(require('./components/app').default);
    });
    module.hot.accept('./reducers', () => {
        store.replaceReducer(require('./reducers').default);
    });
}