export function fetchTickets() {
    return new Promise(resolve => {
        fetch('/fixtures/tickets.json')
            .then(res => res.json())
            .then(json => resolve(json.tickets))
    })
}