import {cloneDeep, each} from 'lodash';
import md5 from 'crypto-js/md5';

export default (state = [], action) => {
    let newState = {};

    switch (action.type) {
        case 'ADD_TICKETS':
            newState = cloneDeep(state);
            each(action.tickets, ticket => {
                let hash = md5(JSON.stringify(ticket)).toString();

                newState[hash] = Object.assign(ticket, {hash});
            });

            return newState;
        default:
            return state
    }
}