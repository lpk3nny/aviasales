import {cloneDeep} from 'lodash';

export default (state = {}, action) => {
    let newState = {};

    switch (action.type) {
        case 'TOGGLE_STOP_COUNT_FILTER':
            newState = cloneDeep(state);
            newState.stopCountStates[action.count] = action.state;

            return newState;
        case 'TOGGLE_ONLY_STOP_COUNT_FILTER':
            newState = cloneDeep(state);
            newState.stopCountStates = state.stopCountStates.map((state, count) => count == action.count);

            return newState;
        case 'TOGGLE_ALL_STOP_COUNT_FILTERS':
            newState = cloneDeep(state);
            let isAllFiltersActive = !state.stopCountStates.filter(filter => filter == false).length;
            newState.stopCountStates = state.stopCountStates.map(state => !isAllFiltersActive);

            return newState;
        case 'UI_TOGGLE_FILTERS':
            newState = cloneDeep(state);
            newState.ui.expanded = !newState.ui.expanded;

            return newState;
        default:
            return state
    }
}