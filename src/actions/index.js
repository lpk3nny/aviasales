export function toggleStopCountFilter(count, state) {
    return {
        type: 'TOGGLE_STOP_COUNT_FILTER',
        count,
        state
    }
}

export function toggleOnlyStopCountFilter(count) {
    return {
        type: 'TOGGLE_ONLY_STOP_COUNT_FILTER',
        count
    }
}

export function toggleAllCountFilters() {
    return {
        type: 'TOGGLE_ALL_STOP_COUNT_FILTERS'
    }
}

export function addTickets(tickets) {
    return {
        type: 'ADD_TICKETS',
        tickets
    }
}

export function UItoggleFilters() {
    console.log('UItoggleFilters')
    return {
        type: 'UI_TOGGLE_FILTERS',
    }
}